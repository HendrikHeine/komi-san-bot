import os
import logging
import json

class Config:
    def __init__(self, logger:logging) -> None:
        self.__configPath = "/app/data/config"
        self.__logger = logger
        self.serverConfigs = []
        self.loadAll()

    def write(self, guildID, messageID:int, channelID:int, roleID:int):
        with open(file=f"{self.__configPath}/{guildID}.json", mode="w") as file:
            data = {"messageID": messageID, "channelID": channelID, "guildID": guildID, "roleID": roleID}
            file.write(str(data))
            file.close()
        self.loadAll()

    def edit(self, guildID, messageID:int, channelID:int):
        pass

    def load(self, fileName):
        with open(file=f"{self.__configPath}/{fileName}", mode="r") as file:
            content = json.loads(file.read().replace("'", '"'))
            file.close()
            return content

    def loadAll(self):
        files = os.listdir(path=f"{self.__configPath}/")
        configFiles = []
        for file in files:
            configFiles.append(self.load(fileName=file))
        self.serverConfigs = configFiles