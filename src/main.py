import secretHandler
import configHandler    
import logging
import datetime
import time as t

import discord
from discord import Status
from discord import app_commands
from discord import Interaction
from discord import Embed
from discord import Guild
from discord import Color
from discord.activity import Game
from discord.ext.commands import has_permissions

botVersion = "1.1.0"
botDate = "06.12.2022"

secret = secretHandler.secret()
token = secret.loadSecret("secret.txt")
if token[0] != 0:
    exit(f"Cannot load token. Error Code {token[0]}")
botToken = token[1]

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='data/log/discord.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('%(asctime)s|%(levelname)s|%(name)s|:%(message)s'))
logger.addHandler(handler)

config = configHandler.Config(logger=logger)
serverConfigs = []
class client(discord.Client):
    async def startup(self):
        await self.wait_until_ready()

def getDateTime():
    dateNow = datetime.datetime.now()
    dateNow = str(dateNow).split(" ")
    unix = t.time()
    time = dateNow[1]
    time = time.split(".")
    time = time[0]
    dateNow = dateNow[0]
    return dateNow, time, unix

startTime = getDateTime()[2]

bot = client(intents=discord.Intents.all())
tree = app_commands.CommandTree(client=bot)


@bot.event
async def on_ready():
    logger.info(f"Logged in as: {bot.user.name} with ID {bot.user.id}")
    await bot.change_presence(activity=Game(name="games with Tadano"), status=Status.online)
    await tree.sync()

@bot.event
async def on_guild_join(guild: Guild):
    logger.info("Added to Guild")
    await guild.system_channel.send("Hii^^")

@bot.event
async def on_raw_reaction_add(payload):
    messageID = payload.message_id
    guildID = payload.guild_id
    emoji = payload.emoji
    userID = payload.user_id
    for conf in config.serverConfigs:
        conf:dict
        if conf['guildID'] == guildID and conf['messageID'] == messageID:
            logger.info("Reaction added")
            guild = discord.utils.find(lambda g: g.id == guildID, bot.guilds)
            user = discord.utils.find(lambda m: m.id == userID, guild.members)
            roleID = conf["roleID"]
            role = discord.utils.get(guild.roles, id=roleID)
            await user.add_roles(role)

@bot.event
async def on_raw_reaction_remove(payload):
    messageID = payload.message_id
    guildID = payload.guild_id
    emoji = payload.emoji
    userID = payload.user_id
    for conf in config.serverConfigs:
        conf:dict
        if conf['guildID'] == guildID and conf['messageID'] == messageID:
            logger.info("Reaction removed")
            guild = discord.utils.find(lambda g: g.id == guildID, bot.guilds)
            user = discord.utils.find(lambda m: m.id == userID, guild.members)
            roleID = conf["roleID"]
            role = discord.utils.get(guild.roles, id=roleID)
            await user.remove_roles(role)

@app_commands.checks.has_permissions(administrator=True)
@tree.command(name="enable", description="Enables the bot")
async def slash(interaction: Interaction):
    guild = interaction.guild
    message = await guild.rules_channel.send("Rules...")
    
    roleFound = False
    roles = await guild.fetch_roles()
    for role in roles:
        if role.name == "✅Regeln akzeptiert":
            roleFound = True
    
    if not roleFound:
        await guild.create_role(name="✅Regeln akzeptiert", mentionable=True, color=Color.green())
        
    roles = await guild.fetch_roles()
    for role in roles:
        if role.name == "✅Regeln akzeptiert":
            config.write(guildID=guild.id, messageID=message.id, channelID=message.channel.id, roleID=role.id)

    await message.add_reaction('✅')
    await interaction.response.send_message(content="Rules set up", ephemeral=True)


@app_commands.checks.has_permissions(administrator=True)
@tree.command(name="edit", description="Edit the Rule Message")
async def slash(interaction: Interaction, new_rules: str):
    guild = interaction.guild
    await interaction.response.send_message(content="Rule will be updated", ephemeral=True)
    for conf in config.serverConfigs:
        conf:dict
        if conf['guildID'] == guild.id:
            channel = discord.utils.find(lambda c: c.id == conf['channelID'], guild.channels)
            message = await channel.fetch_message(conf['messageID'])

            ruleMessageIDs = new_rules.split(",")
            newRules = ""
            for ruleID in ruleMessageIDs:
                newRule = await channel.fetch_message(int(ruleID))
                newRules = newRules + "\n" + newRule.content
            message:discord.Message
            logger.info(newRules)
            
            await message.edit(content=newRules)

@tree.command(name="info", description="Just a bunch of informations about this bot")
async def slash(interaction: Interaction):
    logger.info("Command: info")
    timeNow = getDateTime()[2]
    uptime = timeNow - startTime

    botString = \
        f"""Uptime: {int(round(uptime/60, 1))}m
Version: {botVersion} from {botDate}
Developer: DasMoorhuhn.py#2604
Sourcecode: https://gitlab.com/HendrikHeine/komi-san-bot
Host: Debian 11 Server
Docker: Compose V3
        """

    embed = Embed(title=f"Info", description="about this Bot", timestamp=datetime.datetime.utcnow(), color=Color.blue())
    embed.add_field(name="Bot", value=botString, inline=False)
    await interaction.response.send_message(embed=embed)

@tree.error
async def on_app_command_error(interaction: Interaction, error):
    if isinstance(error, app_commands.MissingPermissions):
        await interaction.response.send_message(content="Du hast keine Adminrechte", ephemeral=True)
    else:
        raise error
    
try:
    bot.run(token=botToken)
except Exception as err:
    raise err
    exit(1)
finally:
    logger.info("Stopped")
