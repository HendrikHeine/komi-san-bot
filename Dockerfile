FROM python:3.8-alpine

WORKDIR /app

COPY requirements.txt requirements.txt
COPY secret.txt /app/secret.txt
RUN pip3 install -r requirements.txt
RUN rm requirements.txt

COPY ./src/ .

RUN mkdir data
RUN mkdir data/log
RUN mkdir data/config

CMD [ "python3", "main.py" ]
